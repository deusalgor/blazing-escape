extends Node

onready var start_block = $StartBlock

var blocks = []
var previous_position = Vector2()
var anchor_y = 0.0

const BLOCKS_POOL = [
	preload("res://scenes/FullIceBlock.tscn"),
	preload("res://scenes/HalfIceBlock.tscn"),
	preload("res://scenes/TrapIceBlock.tscn")
]


func _ready():
	
	anchor_y = start_block.position.y
	blocks.append(start_block)
	previous_position = start_block.position
	randomize()


func remove_block(name):
	
	if blocks.has(name):
		blocks.remove(name)
		spawn_block()


func _physics_process(_delta):
	
	while blocks.size() < 20:
		spawn_block()


func spawn_block():
	
	var block = BLOCKS_POOL[randi() % BLOCKS_POOL.size()].instance()
	call_deferred("add_child", block)
	blocks.append(block.name)
	var x_range = range(300,500)
	var x_offset = x_range[randi() % x_range.size()]
	var y_range = range(-100,100)
	var y_offset = y_range[randi() % y_range.size()]
	var spawn_position = previous_position + Vector2(x_offset,y_offset)
	previous_position = Vector2(spawn_position.x, anchor_y)
	block.position = spawn_position
