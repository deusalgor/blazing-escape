extends KinematicBody2D

signal player_dead

const GRAVITY = 9.8
const JUMP_IMPULSE = 500
const JUMP_COST = 8
const SPEED = 300
const UP = Vector2(0,-1)
const explosion = preload("res://assets/gdquest/Explosion.tscn")

onready var jump_cooldown = $Cooldown
onready var animation = $AnimationPlayer
onready var particles = [$FireCore, $FireTrail, $TrailSparkles, $CoreSparkles]

var on_floor = false
var on_cooldown = false
var motion = Vector2()
var alive = true
var spawn_x = 0


func _ready():
	
	spawn_x = position.x


func _physics_process(delta):
	
	Globals.points = (position.x - spawn_x)/10
	
	if alive and position.y > 525:
		die()
		motion.x = 0
	
	if !is_on_floor():
		apply_gravity()
	else:
		motion.y = 0
		Globals.fuel += 40 * delta
		update_fuel_bar()
		
	if alive:
		listen_controls()
	
	move_and_slide(motion, UP)

func apply_gravity():
	
	motion.y += GRAVITY


func listen_controls():
	
	if Input.is_action_just_pressed("Jump") and can_jump():
		motion.y -= JUMP_IMPULSE
		var jump_explosion = explosion.instance()
		add_child(jump_explosion)
		on_cooldown = true
		Globals.fuel -= JUMP_COST
		update_fuel_bar()
		
		jump_cooldown.start()
		
	
	var direction = int(Input.is_action_pressed("Right")) - int(Input.is_action_pressed("Left"))
	motion.x = direction * SPEED 


func update_fuel_bar():
	
	get_parent().fuel_bar.value = Globals.fuel
	for particle in particles:
		particle.amount = int(Globals.fuel)
	


func can_jump():
	
	return !on_cooldown and Globals.fuel >= JUMP_COST


func _on_Cooldown_timeout():
	
	on_cooldown = false


func die():
	
	emit_signal("player_dead")
	animation.play("Fade")
	alive = false
