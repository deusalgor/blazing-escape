extends StaticBody2D

export (float) var melting_speed = 0.1
export (bool) var trap = false

var melting = false


func _physics_process(delta):
	
	if melting:
		if scale.y > melting_speed:
			scale.y -= melting_speed * delta
		else:
			melting = false
			_remove()


func _on_Explosion_exploded():
	
	_remove()


func _remove():
	
	get_parent().remove_block(name)
	queue_free()


func _on_Area2D_body_entered(body):
	
	if body.name == "FireBall":
		melting = true


func _on_VisibilityNotifier2D_screen_exited():
	
	_remove()
