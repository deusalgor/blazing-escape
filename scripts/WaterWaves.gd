extends ParallaxLayer

export (int) var direction = -1
export (float) var max_offset = 5
export (float) var speed = 5

func _physics_process(delta):
	
	motion_offset.x += direction * speed * delta
	motion_offset.y += direction * speed * delta
	
	match direction:
		-1:
			if motion_offset.x <= -max_offset:
				direction = 1
		1:
			if motion_offset.x >= max_offset:
				direction = -1
